const express = require("express");
const app = express();
const routes = require("./router");
const morgan = require("morgan");
const cors = require("cors");
require("dotenv").config();

/* lists global middleware */
app.use(cors());
app.use(express.json());
if (process.env.NODE_ENV !== "test") app.use(morgan("dev"));

/* root routes */
app.get("/", (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Welcome to to-do list app",
  });
});

/* route middleware */
app.use("/api/v1", routes);

const errorHandler = require("./middlewares/errorHandler");
errorHandler.forEach((handler) => {
  app.use(handler);
});

module.exports = app;
