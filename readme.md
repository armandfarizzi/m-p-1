# Backend Task for Mini Project

Setup : 

1. install dependencies using ```$ yarn add``` 
2. rename ```.env.example``` into ```.env```
3. fill the correct credentials (DB_Username, DB_password) in your ```.env```, You can specify any name for DB_name
4. run ```$ sequelize db:create```
5. run ```$ sequelize db:migrate```
6. Happy Hacking!

## TO-DO

Armand : Creating aLl routes