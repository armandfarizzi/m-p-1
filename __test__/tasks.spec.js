const app = require("../index");
const request = require("supertest");
const { task } = require("../models");
const { getToken1, getToken2 } = require("./utils");

describe("Task Endpoint", () => {
  beforeAll(async () => {
    await task.destroy({
      truncate: true,
      restartIdentity: true,
    });
  });

  describe("Task API", () => {
    test("should not get tasks if not authenticated", (done) => {
      request(app)
        .get("/api/v1/tasks")
        .then((res) => {
          expect(res.statusCode).toBe(401);
          done();
        });
    });

    test("should fail to create a new task if not authenticated", (done) => {
      request(app)
        .post("/api/v1/tasks")
        .set("Content-Type", "application/json")
        .send({
          name: "task armand nol",
          description: "lusa",
          deadline: "03/03/2020",
        })
        .then((res) => {
          expect(res.statusCode).toBe(401);
          done();
        });
    });

    test("should success create a new task if authenticated", (done) => {
      getToken1()
        .then(async ({ access_token, id }) => {
          await request(app)
            .post("/api/v1/tasks")
            .set("Content-Type", "application/json")
            .set("Authorization", access_token)
            .send({
              name: "task armand nol",
              description: "lusa",
              deadline: "03/03/2020",
            });
          request(app)
            .post("/api/v1/tasks")
            .set("Content-Type", "application/json")
            .set("Authorization", access_token)
            .send({
              name: "task armand",
              description: "lusa",
              deadline: "03/03/2020",
            })
            .then(async (res) => {
              expect(res.statusCode).toBe(201);
              const allTasks = await task.findAll({ where: { user_id: id } });
              allTasks.forEach((t) => {
                expect(t.user_id).toBe(id);
              });
              done();
            });
        })
        .catch((error) => {
          console.log(error.message);
        });
    });

    test("should fail create a new task if name and deadline field is empty/null", (done) => {
      getToken1()
        .then(async ({ access_token }) => {
          const res1 = await request(app)
            .post("/api/v1/tasks")
            .set("Content-Type", "application/json")
            .set("Authorization", access_token)
            .send({
              deadline: "",
            });
          expect(res1.statusCode).toBe(400);
          expect(res1.body.error).toMatch("null");
          done();
        })
        .catch((error) => {
          console.log(error.message);
        });
    });

    test("should fail create a new task if request body having a bad key ", (done) => {
      getToken1()
        .then(async ({ access_token }) => {
          const res1 = await request(app)
            .post("/api/v1/tasks")
            .set("Content-Type", "application/json")
            .set("Authorization", access_token)
            .send({
              naameee: "nameeee is it",
              deaaadldddine: "23/05/2020",
            });
          expect(res1.statusCode).toBe(400);
          expect(res1.body.status).toEqual("failed");
          expect(res1.body.error).toMatch("null");
          done();
        })
        .catch((error) => {
          console.log(error.message);
        });
    });

    test(`should get all task if authenticated and all task should match with their id`, (done) => {
      getToken1()
        .then(({ access_token: token, id }) => {
          request(app)
            .get("/api/v1/tasks")
            .set("Authorization", token)
            .then((res) => {
              expect(res.statusCode).toBe(200);
              res.body.data.forEach((element) => {
                expect(element.user_id).toEqual(id);
              });
              expect(res.body.data.length).toBe(2);
              done();
            });
        })
        .catch((error) => {
          console.log(error.message);
        });
    });
    test("Should successfully get a certain task with ID if authenticated", (done) => {
      getToken1().then(({ access_token: token, id }) => {
        request(app)
          .get("/api/v1/tasks/1")
          .set("Authorization", token)
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body.data.user_id).toBe(id);
            done();
          });
      });
    });
    test("Should fail to get a task with a invalid id", (done) => {
      getToken1().then(({ access_token: token, id }) => {
        request(app)
          .get("/api/v1/tasks/5")
          .set("Authorization", token)
          .then((res) => {
            console.log(res.body);
            expect(res.statusCode).toBe(404);
            expect(res.body.data).toBeFalsy();
            done();
          });
      });
    });
    test(`should not get tasks if not authenticated`, (done) => {
      request(app)
        .get("/api/v1/tasks")
        .then((res) => {
          expect(res.statusCode).toBe(401);
          done();
        });
    });

    test("should fail to delete a task if authenticated and but did not having same id at task user_id", (done) => {
      getToken2()
        .then(({ access_token }) => {
          request(app)
            .delete("/api/v1/tasks/1")
            .set("Authorization", access_token)
            .then((res) => {
              expect(res.body.status).toBe("failed");
              expect(res.statusCode).toBe(403);
              done();
            });
        })
        .catch((error) => {
          console.log(error.message);
        });
    });

    test("should successfully modify a task if authenticated and authorized", (done) => {
      getToken1()
        .then(async ({ access_token }) => {
          const beforeModify = await task.findByPk(1);

          const res = await request(app)
            .put("/api/v1/tasks/1")
            .set("Authorization", access_token)
            .set("Content-Type", "application/json")
            .send({
              name: "pokoknya harus di update",
              deadline: "03/06/2020 03:00",
            });
          expect(res.statusCode).toEqual(200);
          expect(res.body.data.name).toEqual("pokoknya harus di update");
          const afterModify = await task.findByPk(1);

          expect(beforeModify.name).not.toEqual(afterModify.name);
          done();
        })
        .catch((error) => {
          console.log(error.message);
        });
    });

    test("should successfully delete a task if authenticated and authorized", (done) => {
      getToken1()
        .then(async ({ access_token }) => {
          const beforeDelete = await task.findByPk(1);
          expect(beforeDelete).not.toBeFalsy();
          request(app)
            .delete("/api/v1/tasks/1")
            .set("Authorization", access_token)
            .then(async (res) => {
              expect(res.body.message).toEqual("Successfully deleted a task");
              expect(res.body.status).toBe("success");
              expect(res.statusCode).toBe(200);
              const deleted = await task.findByPk(1);
              expect(deleted).toBeFalsy();
              done();
            });
        })
        .catch((error) => {
          console.log(error.message);
        });
    });
  });
});
