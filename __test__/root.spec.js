const app = require("../index");
const request = require("supertest");

describe("Root Endpoint", () => {
  describe("GET /", () => {
    test("Should return 200", (done) => {
      request(app)
        .get("/")
        .then((res) => {
          expect(res.body.status).toEqual("success");
          expect(res.body.message).toEqual("Welcome to to-do list app");
          done();
        });
    });
  });
});
