const app = require("../index");
const request = require("supertest");
const { user } = require("../models");
const { getToken1 } = require("./utils");
const path = require("path");
const Imagekit = require("imagekit");

describe("User Endpoint", () => {
  beforeAll(async () => {
    await user.destroy({
      truncate: true,
      cascade: true,
      restartIdentity: true,
    });
    return;
  });

  describe("Users API test case \n", () => {
    test("Should failed to create a new account when password is less than 8 long characters", (done) => {
      request(app)
        .post("/api/v1/register")
        .set("Content-Type", "application/json")
        .send({
          email: "armandfarizzi@gmail.com",
          name: "armand",
          password: "armanda",
        })
        .then(async (res) => {
          expect(res.body.status).toBe("failed");
          expect(res.body.error).toMatch("characters");
          done();
        });
    });
    test("Should created a new account & provide jwt on respose", (done) => {
      request(app)
        .post("/api/v1/register")
        .set("Content-Type", "application/json")
        .send({
          email: "armandfarizzi@gmail.com",
          name: "armand",
          password: "armandarmand",
        })
        .then(async (res) => {
          expect(res.body.status).toBe("success");
          expect(res.statusCode).toBe(201);
          expect(res.body).not.toBeFalsy();
          expect(res.body.data.access_token).toBeDefined();
          const created = await user.findOne({ where: { name: "armand" } });
          expect(created.name).toEqual("armand");
          done();
        });
    });
    test("Should not created a new account with same email", (done) => {
      request(app)
        .post("/api/v1/register")
        .set("Content-Type", "application/json")
        .send({
          email: "armandfarizzi@gmail.com",
          name: "armand",
          password: "armandarmand",
        })
        .then((res) => {
          expect(res.body.status).toBe("failed");
          expect(res.statusCode).toBe(422);
          done();
        });
    });
    test("Should get a new token", (done) => {
      request(app)
        .post("/api/v1/login")
        .set("Content-Type", "application/json")
        .send({
          email: "armandfarizzi@gmail.com",
          password: "armandarmand",
        })
        .then((res) => {
          expect(res.body.status).toBe("success");
          done();
        });
    });
    test("Should fail to get user identity when authenticated", (done) => {
      request(app)
        .get("/api/v1/user")
        .then((res) => {
          expect(res.statusCode).toEqual(401);
          done();
        });
    });
    test("Should success to get user identity when authenticated", async (done) => {
      const { access_token, image_url } = await getToken1();
      request(app)
        .get("/api/v1/user")
        .set("Authorization", access_token)
        .then((res) => {
          expect(res.statusCode).toEqual(200);
          expect(res.body.data.image_url).toEqual(image_url);
          done();
        });
    });

    test("Should success to update user image_url identity when authenticated", async () => {
      const { access_token, image_url } = await getToken1();
      const res = await request(app)
        .post("/api/v1/user/avatar")
        .set("Authorization", access_token)
        .attach("image", path.resolve(__dirname, "ask.png"));
      expect(res.statusCode).toEqual(200);
      expect(res.body.data.image_url).not.toEqual(image_url);
      console.log("imagekit mocked?", await Imagekit.prototype.upload.mock);
      return;
    });
    test("Should fail to access upload user avatar if not authenticated with respond status equal 401", (done) => {
      request(app)
        .post("/api/v1/user/avatar")
        .attach("image", __dirname + "/ask.png")
        .then((res) => {
          expect(res.statusCode).toEqual(401);
          done();
        });
    });
    test("Should failed to access upload user avatar when bad token is given", (done) => {
      request(app)
        .post("/api/v1/user/avatar")
        .set("Authorization", "BAD TOKEN HERE")
        .attach("image", __dirname + "/ask.png")
        .then(async (res) => {
          expect(res.statusCode).toEqual(401);
          done();
        });
    });
  });
});
