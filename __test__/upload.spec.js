const app = require("../index");
const supertest = require("supertest");
const request = supertest(app);
const path = require("path");
const Imagekit = require("imagekit");
const { getToken1 } = require("./utils");

function mockRequest() {
  return {
    file: Buffer.alloc(10),
  };
}

// Factory Function
function mockResponse() {
  const res = {};
  res.user = { image_url: jest.fn().mockReturnValue(res), save: () => {} };
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
}

describe("Here i'm trying to mock an Imagekit module, it's upload to be precisely", () => {
  it("should expect something", async () => {
    const { access_token } = await getToken1();
    await request
      .post("/api/v1/user/avatar")
      .set("Authorization", access_token)
      .attach("image", path.resolve(__dirname, "ask.png"));

    console.log(await Imagekit.prototype.upload.mock.results[0].value);
    expect(Imagekit.prototype.upload.mock.calls.length).toBe(1);
  });
});
