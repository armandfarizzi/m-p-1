const { user } = require("../models");
const app = require("../index");
const request = require("supertest");

const getToken1 = async function () {
  try {
    await user.findOrCreate({
      where: { email: "armandfarizzi@gmail.com" },
      defaults: {
        password: "armandarmand",
      },
    });
    return request(app)
      .post("/api/v1/login")
      .set("Content-Type", "application/json")
      .send({
        email: "armandfarizzi@gmail.com",
        password: "armandarmand",
      })
      .then((res) => {
        return res.body.data;
      })
      .catch((error) => {
        console.log(error.message);
      });
  } catch (error) {
    console.log(error.message);
  }
};

// const mockUpload = async function () {};

const getToken2 = async function () {
  try {
    await request(app)
      .post("/api/v1/register")
      .set("Content-Type", "application/json")
      .send({
        email: "testes@gmail.com",
        name: "tes",
        password: "testestes",
      });
  } catch (error) {
    console.log(error.message);
  }
  return request(app)
    .post("/api/v1/login")
    .set("Content-Type", "application/json")
    .send({
      email: "testes@gmail.com",
      password: "testestes",
    })
    .then((res) => {
      return res.body.data;
    });
};

module.exports = { getToken1, getToken2 };
