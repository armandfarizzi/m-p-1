const responseHandler = function (req, res, next) {
  res.status(res.statusCode || 200).json({
    status: "success",
    data: res.data,
    message: res.message,
    metadata: res.metadata,
  });
};

module.exports = responseHandler;
