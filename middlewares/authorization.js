const models = require("../models");

const authorization = (modelname) => {
  const UsedModel = models[modelname];
  return function (req, res, next) {
    UsedModel.findByPk(req.params.id)
      .then((foundCtx) => {
        if (!foundCtx) {
          res.statusCode = 404;
          next(new Error(`No ${modelname} found!`));
          return;
        }
        if (foundCtx.user_id !== res.user.id) {
          res.statusCode = 403;
          next(new Error("You are not allowed to do this!"));
          return;
        }
        next();
      })
      .catch((err) => {
        res.statusCode = 400;
        next(err);
      });
  };
};

module.exports = authorization;
