const { user } = require("../models");
const { verify } = require("jsonwebtoken");
require("dotenv").config();

const authentication = function (req, res, next) {
  if (!req.headers.authorization) {
    res.statusCode = 401;
    next(new Error("Authentication failed, please login to access"));
    return;
  }
  const token = req.headers.authorization;
  try {
    const payload = verify(token, process.env.SECRETKEY);
    user
      .findByPk(payload.id)
      .then((foundUser) => {
        res.user = foundUser;
        next();
      })
      .catch((err) => {
        next(err);
      });
  } catch (error) {
    res.statusCode = 401;
    next(
      new Error("Authentication failed, please login with correct credentials")
    );
  }
};

module.exports = authentication;
