const errorHandler = [
  function (req, res, next) {
    res.status(404);
    next(new Error("Not Found"));
  },
  function (err, req, res, next) {
    if (res.statusCode == 200) {
      res.status(500);
    }
    res.json({ status: "failed", error: err.message });
  },
];

module.exports = errorHandler;
