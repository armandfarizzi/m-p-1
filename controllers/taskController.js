const { task } = require("../models");
const moment = require("moment");

const filterByArrayFields = (obj, arr) => {
  const filtered = { ...obj };
  for (const field in filtered) {
    if (arr.indexOf(field) == -1) {
      delete filtered[field];
    }
  }
  return filtered;
};

const queryBuilder = (obj) => {
  let str = "";
  for (const key in obj) {
    str += `&${key}=${obj[key]}`;
  }
  return str;
};

const taskController = {
  add: (req, res, next) => {
    // task.create()
    const userId = { user_id: res.user.id };
    req.body.deadline = moment(
      req.body.deadline,
      "DD/MM/YYYY hh:mm:ss"
    ).toJSON();
    task
      .create({ ...userId, ...req.body })
      .then((ctx) => {
        res.data = ctx;
        res.statusCode = 201;
        next();
      })
      .catch((error) => {
        res.statusCode = 400;
        next(error);
      });
  },
  show: (req, res, next) => {
    // if path parameter is defined in request
    // Function will return single task
    // with corresponding Id
    // --Only return self owned task--
    if (req.params.id) {
      return task
        .findOne({ where: { id: req.params.id, user_id: res.user.id } })
        .then((ctx) => {
          if (!ctx) {
            res.statusCode = 404;
            return next(new Error("No task found!"));
          }
          res.data = ctx;
          return next();
        })
        .catch((error) => {
          res.statusCode(404);
          return next(error);
        });
    }

    const filterFields = ["completed", "important"];
    const page = req.query.page || 1;
    const filter = filterByArrayFields({ ...req.query }, filterFields);
    const pagination = { limit: 10, offset: (page - 1) * 10 };

    task
      .findAndCountAll({
        where: {
          user_id: res.user.id,
          ...filter,
        },
        order: ["deadline"],
        ...pagination,
      })
      .then(({ count, rows }) => {
        res.data = rows;
        res.metadata = {
          countALl: count,
          next:
            page * 10 < count
              ? `${process.env.HEROKU_URL}tasks?page=${
                  Number(page) + 1
                }${queryBuilder(filter)}`
              : null,
          previous:
            page > 1
              ? `${process.env.HEROKU_URL}tasks?page=${page - 1}${queryBuilder(
                  filter
                )}`
              : null,
        };
        next();
      })
      .catch((err) => {
        res.statusCode = 400;
        next(err);
      });
  },
  finish: (req, res, next) => {
    task.findByPk(req.params.id).then((foundTask) => {
      foundTask.set("completed", !foundTask.completed).save();
      res.data = foundTask;
      next();
    });
  },
  important: (req, res, next) => {
    task.findByPk(req.params.id).then((foundTask) => {
      foundTask.set("important", !foundTask.important).save();
      res.data = foundTask;
      next();
    });
  },
  update: (req, res, next) => {
    task
      .update(req.body, {
        where: { id: req.params.id },
      })
      .then(async () => {
        res.data = await task.findByPk(req.params.id);
        res.statusCode = 200;
        next();
      })
      .catch((err) => {
        res.statusCode = 400;
        next(err);
      });
  },
  del: (req, res, next) => {
    task
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(() => {
        res.statusCode = 200;
        res.message = "Successfully deleted a task";
        next();
      })
      .catch((err) => {
        res.statusCode = 400;
        next(err);
      });
  },
};

module.exports = taskController;
