const { user } = require("./../models");
const ImageKit = require("../lib/imagekit");

const userController = {
  me: (req, res, next) => {
    res.data = res.user.entity;
    next();
  },
  register: async (req, res, next) => {
    try {
      const newUser = await user.create(req.body);
      res.statusCode = 201;
      res.data = newUser.entity;
      next();
    } catch (error) {
      res.statusCode = 422;
      next(error);
    }
  },
  logout: (req, res) => {
    res.status(200).json({
      status: "success",
      message: "logout api",
    });
  },
  login: async (req, res, next) => {
    try {
      const instance = await user.findOne({ where: { email: req.body.email } });
      if (!instance.checkCredentials(req.body.password)) {
        res.statusCode = 401;
        next(new Error("Email or password may be incorrect"));
        return;
      }
      res.data = instance.entity;
      next();
    } catch (error) {
      res.status(400).json({ status: "failed", message: error.message });
    }
  },
  update: async (req, res, next) => {
    try {
      const blacklist = ["email", "password", "id"];
      for (let keys in req.body) {
        if (blacklist.indexOf(keys) == -1) {
          res.user[keys] = req.body[keys];
        }
      }
      await res.user.save();
      res.data = res.user.entity;
      next();
    } catch (error) {
      res.status(400);
      next(error);
    }
  },
  uploadAvatar: async (req, res, next) => {
    if (process.env.NODE_ENV == "development") {
      res.user.image_url = req.file.url;
      await res.user.save();
      res.data = res.user;
      next();
      return;
    }
    try {
      const upload = await ImageKit.upload({
        file: req.file.buffer,
        fileName: req.file.originalname,
      });

      res.user.image_url = upload.url;
      await res.user.save();
      res.data = upload;
      next();
    } catch (error) {
      console.log(error.message);
      res.statusCode = 422;
      next(error);
    }
  },
};

module.exports = userController;
