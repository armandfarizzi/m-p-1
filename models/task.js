"use strict";
module.exports = (sequelize, DataTypes) => {
  const task = sequelize.define(
    "task",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            args: true,
            msg: "Task name can not be null",
          },
          notEmpty: {
            args: true,
            msg: "Task name can not be empty",
          },
        },
      },
      description: DataTypes.TEXT,
      deadline: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
          notNull: {
            args: true,
            msg: "Task deadline can not be null",
          },
          notEmpty: {
            args: true,
            msg: "Task deadline can not be empty",
          },
        },
      },
      important: DataTypes.BOOLEAN,
      completed: DataTypes.BOOLEAN,
      user_id: DataTypes.INTEGER,
    },
    {}
  );
  task.associate = function (models) {
    task.belongsTo(models.user, {
      foreignKey: "user_id",
    });
  };
  return task;
};
