"use strict";

const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    "user",
    {
      name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: "Email has already been taken",
        },
        validate: {
          isEmail: {
            args: true,
            msg: "Invalid email",
          },
        },
      },
      image_url: {
        type: DataTypes.TEXT,
        validate: {
          isUrl: {
            args: true,
            msg: "Invalid url for user's image_url",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: {
            args: 8,
            msg: "Password must be at least 8 characters",
          },
        },
      },
    },
    {
      hooks: {
        beforeCreate: function (instance) {
          const encrypted_password = bcrypt.hashSync(
            instance.password,
            bcrypt.genSaltSync(10)
          );
          instance.password = encrypted_password;
          instance.image_url = gravatar.url(
            instance.email,
            { s: "100", r: "x" },
            true
          );
        },
      },
    }
  );
  Object.defineProperty(user.prototype, "entity", {
    get() {
      return {
        id: this.id,
        email: this.email,
        name: this.name,
        image_url: this.image_url,

        access_token: this.getToken(),
      };
    },
  });
  user.associate = function (models) {
    // associations can be defined here
    user.hasMany(models.task, {
      foreignKey: "user_id",
    });
  };
  user.prototype.getToken = function () {
    return jwt.sign(
      {
        id: this.id,
        email: this.email,
        image_url: this.image_url,
      },
      process.env.SECRETKEY,
      { expiresIn: "1d" }
    );
  };
  user.prototype.checkCredentials = function (password) {
    return bcrypt.compareSync(password, this.password);
  };
  return user;
};
