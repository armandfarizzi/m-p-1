const router = require("express").Router();

const userController = require("./controllers/userController");
const taskController = require("./controllers/taskController");
const responseHandler = require("./middlewares/responseHandler");
const authenticate = require("./middlewares/authentication");
const authorize = require("./middlewares/authorization");

const fileHandler = require("./middlewares/fileHandler");
/*
 *  Base routes : "/api/v1"
 *
 * */

// user api collections
router.post("/login", userController.login, responseHandler);
router.post("/logout", userController.logout);
router.post("/register", userController.register, responseHandler);
router.put("/user", authenticate, userController.update, responseHandler);
router.get("/user", authenticate, userController.me, responseHandler);

// task api collections
router.get("/tasks/:id?", authenticate, taskController.show, responseHandler);
router.post("/tasks", authenticate, taskController.add, responseHandler);
router.patch(
  "/tasks/:id/finish",
  authenticate,
  authorize("task"),
  taskController.finish,
  responseHandler
);
router.patch(
  "/tasks/:id/important",
  authenticate,
  authorize("task"),
  taskController.important,
  responseHandler
);
router.put(
  "/tasks/:id",
  authenticate,
  authorize("task"),
  taskController.update,
  responseHandler
);
router.delete(
  "/tasks/:id",
  authenticate,
  authorize("task"),
  taskController.del,
  responseHandler
);

//  upload file controller
router.post(
  "/user/avatar",
  authenticate,
  fileHandler.single("image"),
  userController.uploadAvatar,
  responseHandler
);

module.exports = router;
