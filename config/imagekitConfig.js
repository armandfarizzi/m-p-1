require("dotenv").config();

module.exports = {
  publicKey: process.env.IMAGEKIT_PUBLICKEY,
  privateKey: process.env.IMAGEKIT_SECRETKEY,
  urlEndpoint: process.env.IMAGEKIT_URL_ENDPOINT,
};
